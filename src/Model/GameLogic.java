package Model;

import java.util.Random;

public class GameLogic {
	private int randomNumber;
	private int numberOfGuesses;
	private final int minimumNumber = 0;
	private final int maximumNumber = 1000;
	
	public GameLogic(){
		this.numberOfGuesses = 1;
		setRandomNumber();
	}
	
	private void setRandomNumber(){
		Random random = new Random();
		this.randomNumber = random.nextInt(maximumNumber + minimumNumber) - minimumNumber;
	}

	public int getRandomNumber() {
		return randomNumber;
	}

	public int getNumberOfGuesses() {
		return numberOfGuesses;
	}

	public int getMinimumNumber() {
		return minimumNumber;
	}

	public int getMaximumNumber() {
		return maximumNumber;
	}
	
	public String guess(int number){
		numberOfGuesses++;
		if(number < randomNumber){
			return "Wrong. Guess higher.";
		}
		else if(number == randomNumber){
			return "Correct. You needed :" + numberOfGuesses + " guesses.";
		}
		else{
			return "Wrong. Guess lower.";
		}
	}
}
